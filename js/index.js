var ms = document.getElementsByTagName("td");
var result = document.getElementById("result");
var cross_arrow = document.getElementById("cross_arrow");
var circle_arrow = document.getElementById("circle_arrow");
var navbar = document.getElementsByTagName("li");
var header = document.getElementsByTagName("header")[0];
var play_but = document.getElementById("play_but");
var reset = navbar[0];
var with_ai = document.getElementById("with_ai")
var below_but = document.getElementById("below_but");
var rand = 0;
var count = 0;
var turn = false;
var playing_with_ai = false;

header.style.display="none";
cross_arrow.style.display="inline";


with_ai.addEventListener("click",function(){
	playing_with_ai=true;
	document.getElementById("status").textContent = "PLAYING WITH AI";
})

reset.addEventListener("click",function(){
	window.location.reload();
})

play_but.addEventListener("click",function(){
	document.getElementById("play_div").style.display = "none";
	below_but.style.visibility = "hidden";
	header.style.display = "grid";
	for(var i = 0 ; i<ms.length ; i++)
	{
		ms[i].addEventListener("click",gameStart)
	}
})


function check(a,b){
	var ar = ["cross","circle"];
	var value = ar[b];
	var sum = 0;
	for(var i=0;i<a.length;i++){
		if (ms[a[i]].classList.value===value){
			sum+=1;
		}
	}
	if(sum===3){
		return true;
	}
}

function ai_play(){
	if(playing_with_ai){
		var flag = true;
		while(flag){
		rand = Math.floor(Math.random() * 9);
		if(ms[rand].classList.value === ""){
				ms[rand].style.background='url("images/circle.png")';
				ms[rand].classList="circle";
				count+=1;
				circle_arrow.style.display="none";
				cross_arrow.style.display="inline";
				flag=false;
				turn=!turn;
		}
	}
}}

function gameStart(){
		if(!this.style.background)
		{
		if(turn){
			if((!playing_with_ai)&&(this.classList.value==="")){
				this.style.background='url("images/circle.png")';
				this.classList="circle";
				count+=1;
				circle_arrow.style.display="none";
				cross_arrow.style.display="inline";
			}			
		}
		else if(this.classList.value==="" && !turn){
			this.style.background='url("images/cross.png")';
			this.classList="cross";
			count+=1;
			cross_arrow.style.display="none";
			circle_arrow.style.display="inline";
		}
		if(winCheck())
		{
			alert("Complete" + turn)
		}
		turn=!turn;
		ai_play();
		}
}

function stopGame(){
    for(var i=0;i<ms.length;i++)
    {
        ms[i].removeEventListener("click", gameStart);
        
	}
	setTimeout(function(){
		window.location.reload()
	},5000);
}


function winCheck()
{
    if(check([0,1,2],0)||check([0,3,6],0) || check([0,4,8],0)|| check([4,1,7],0)||  check([5,8,2],0)||  check([6,4,2],0)||  check([3,4,5],0) ||  check([7,6,8],0))
    {
		result.textContent = "Cross Wins!!!"
        stopGame();
    }
    
    else if(check([0,1,2],1)||check([0,3,6],1) || check([0,4,8],1)|| check([4,1,7],1)||  check([5,8,2],1)||  check([6,4,2],1)||  check([3,4,5],1) ||  check([7,6,8],1))
    {
		result.textContent = "Circle Wins!!!"
        stopGame();
    }
    
    else if(count===9)
    {
		result.textContent = "Draw!!!"
		stopGame();
    }
}
